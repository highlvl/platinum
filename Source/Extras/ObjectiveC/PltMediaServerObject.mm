//
//  PltMediaServerObject.mm
//  Platinum
//
//  Created by Sylvain on 9/14/10.
//  Copyright 2010 Plutinosoft LLC. All rights reserved.
//

#import "Platinum.h"
#import "PltMediaServerObject.h"
#import "PltMediaConnect.h"
#import "PltMediaRenderer.h"

/*----------------------------------------------------------------------
|   PLT_MediaServerDelegate_Wrapper
+---------------------------------------------------------------------*/
class PLT_MediaServerDelegate_Wrapper : public PLT_MediaServerDelegate {
public:
    PLT_MediaServerDelegate_Wrapper(PLT_MediaServerObject* target) : m_Target(target) {}
    
    NPT_Result OnBrowseMetadata(PLT_ActionReference&          action, 
                                const char*                   object_id, 
                                const char*                   filter,
                                NPT_UInt32                    starting_index,
                                NPT_UInt32                    requested_count,
                                const char*                   sort_criteria,
                                const PLT_HttpRequestContext& context) {
        if (![m_Target.delegate respondsToSelector:@selector(onBrowseMetadata:)])
            return NPT_FAILURE;
        
        PLT_MediaServerBrowseCapsule* capsule = 
            [[PLT_MediaServerBrowseCapsule alloc] initWithAction:action.AsPointer()
                                                        objectId:object_id
                                                          filter:filter
                                                           start:starting_index
                                                           count:requested_count
                                                            sort:sort_criteria
                                                         context:(PLT_HttpRequestContext*)&context];
        NPT_Result result = [m_Target.delegate onBrowseMetadata:capsule];
        
        return result;
    }
    
    NPT_Result OnBrowseDirectChildren(PLT_ActionReference&          action, 
                                      const char*                   object_id, 
                                      const char*                   filter,
                                      NPT_UInt32                    starting_index,
                                      NPT_UInt32                    requested_count,
                                      const char*                   sort_criteria, 
                                      const PLT_HttpRequestContext& context) {
        if (![m_Target.delegate respondsToSelector:@selector(onBrowseDirectChildren:)]) 
            return NPT_FAILURE;
        
        PLT_MediaServerBrowseCapsule* capsule = 
            [[PLT_MediaServerBrowseCapsule alloc] initWithAction:action.AsPointer()
                                                        objectId:object_id
                                                          filter:filter
                                                           start:starting_index
                                                           count:requested_count
                                                            sort:sort_criteria
                                                         context:(PLT_HttpRequestContext*)&context];
        NPT_Result result = [m_Target.delegate onBrowseDirectChildren:capsule];
        
        return result;
    }
    
    NPT_Result OnSearchContainer(PLT_ActionReference&          action, 
                                 const char*                   container_id, 
                                 const char*                   search_criteria,
                                 const char*                   filter,
                                 NPT_UInt32                    starting_index,
                                 NPT_UInt32                    requested_count,
                                 const char*                   sort_criteria, 
                                 const PLT_HttpRequestContext& context) {
        if (![m_Target.delegate respondsToSelector:@selector(onSearchContainer:)]) 
            return NPT_FAILURE;
        
        PLT_MediaServerSearchCapsule* capsule = 
            [[PLT_MediaServerSearchCapsule alloc] initWithAction:action.AsPointer()
                                                        objectId:container_id
                                                          search:search_criteria
                                                          filter:filter
                                                           start:starting_index
                                                           count:requested_count
                                                            sort:sort_criteria
                                                         context:(PLT_HttpRequestContext*)&context];
        NPT_Result result = [m_Target.delegate onSearchContainer:capsule];
        
        return result;
    }
    
    NPT_Result ProcessFileRequest(NPT_HttpRequest&              request,
                                  const NPT_HttpRequestContext& context,
                                  NPT_HttpResponse&             response) {
        PLT_HttpRequestContext _context(request, context);
        PLT_MediaServerFileRequestCapsule* capsule = 
            [[PLT_MediaServerFileRequestCapsule alloc] initWithResponse:&response
                                                                context:&_context];
        NPT_Result result = [m_Target.delegate onFileRequest:capsule];
        
        return result;
    }

private:
    PLT_MediaServerObject* m_Target;
};


/*----------------------------------------------------------------------
 |   PLT_MediaRendererDelegate_Wrapper
 +---------------------------------------------------------------------*/
class PLT_MediaRendererDelegate_Wrapper : public PLT_MediaRendererDelegate {
public:
    PLT_MediaRendererDelegate_Wrapper(PLT_MediaRendererObject* target) : m_Target(target) {}
 
    void OnConnectionEstablished(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onConnectionEstablished:)])
            return;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        [m_Target.delegate onConnectionEstablished:capsule];
        
        return;
    }

    
    NPT_Result OnGetCurrentConnectionInfo(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onGetCurrentConnectionInfo:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onGetCurrentConnectionInfo:capsule];
        
        return result;
    }
    
    NPT_Result OnNext(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }

    NPT_Result OnPause(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onPause:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onPause:capsule];
        
        return result;
    }

    NPT_Result OnPlay(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onPlay:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onPlay:capsule];
        
        return result;
    }

    NPT_Result OnPrevious(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }

    NPT_Result OnSeek(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSeek:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSeekCapsule* capsule = [[PLT_MediaRendererSeekCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSeek:capsule];
        
        return result;
    }

    NPT_Result OnStop(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onStop:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onStop:capsule];
        
        return result;
    }

    NPT_Result OnSetAVTransportURI(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetAVTransportUri:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetAVTransportUriCapsule* capsule = [[PLT_MediaRendererSetAVTransportUriCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetAVTransportUri:capsule];
        
        return result;
    }

    NPT_Result OnSetPlayMode(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetPlayMode:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererGeneralCapsule* capsule = [[PLT_MediaRendererGeneralCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetPlayMode:capsule];
        
        return result;
    }

    NPT_Result OnSetVolume(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }

    NPT_Result OnSetVolumeDB(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }

    NPT_Result OnGetVolumeDBRange(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }

    NPT_Result OnSetMute(PLT_ActionReference& action) {
        if (![m_Target.delegate respondsToSelector:@selector(onSetMute:)])
            return NPT_FAILURE;
        
        PLT_MediaRendererSetMuteCapsule* capsule = [[PLT_MediaRendererSetMuteCapsule alloc] initWithAction:action.AsPointer()];
        NPT_Result result = [m_Target.delegate onSetMute:capsule];
        
        return result;
    }
    
private:
    PLT_MediaRendererObject* m_Target;
};


/*----------------------------------------------------------------------
 |   PLT_MediaRendererGeneralCapsule
 +---------------------------------------------------------------------*/
@implementation PLT_MediaRendererGeneralCapsule

- (id)initWithAction:(PLT_Action*)action
{
    if ((self = [super initWithAction:action])) {
    }
    return self;
}

@end


/*----------------------------------------------------------------------
 |   PLT_MediaRendererSetMuteCapsule
 +---------------------------------------------------------------------*/
@implementation PLT_MediaRendererSetMuteCapsule

@synthesize desiredMute;

- (id)initWithAction:(PLT_Action*)action
{
    if ((self = [super initWithAction:action])) {
        NPT_String tempMute;
        
        action->GetArgumentValue("DesiredMute",  tempMute);
        
        if([[NSString stringWithCString:tempMute encoding:NSUTF8StringEncoding] isEqual:@"1"]){
            desiredMute = YES;
        }else {
            desiredMute = NO;
        }
    }
    return self;
}

@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererSetAVTransportUriCapsule
 +---------------------------------------------------------------------*/
@implementation PLT_MediaRendererSetAVTransportUriCapsule

@synthesize uri;

- (id)initWithAction:(PLT_Action*)action
{
    if ((self = [super initWithAction:action])) {
        NPT_String tempUri;        
        action->GetArgumentValue("CurrentURI",  tempUri);
        uri = [NSString stringWithCString:tempUri.GetChars() encoding:NSUTF8StringEncoding];
    }
    return self;
}

@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererSeekCapsule
 +---------------------------------------------------------------------*/
@implementation PLT_MediaRendererSeekCapsule

@synthesize target;

- (id)initWithAction:(PLT_Action*)action
{
    if ((self = [super initWithAction:action])) {
        NPT_String tempTarget;
        
        action->GetArgumentValue("Target",  tempTarget);
        target = [NSString stringWithCString:tempTarget.GetChars() encoding:NSUTF8StringEncoding];
    }
    return self;
}

@end


/*----------------------------------------------------------------------
|   PLT_MediaServerBrowseCapsule
+---------------------------------------------------------------------*/
@implementation PLT_MediaServerBrowseCapsule

@synthesize objectId, filter, start, count, sort;

- (id)initWithAction:(PLT_Action*)action objectId:(const char*)_id filter:(const char*)_filter start:(NPT_UInt32)_start count:(NPT_UInt32)_count sort:(const char*)_sort context:(PLT_HttpRequestContext*)_context
{
    if ((self = [super initWithAction:action])) {
        objectId = [[NSString alloc] initWithCString:_id encoding:NSUTF8StringEncoding];
        filter   = [[NSString alloc] initWithCString:(_filter==NULL)?"":_filter
                                            encoding:NSUTF8StringEncoding];
        sort     = [[NSString alloc] initWithCString:(_sort==NULL)?"":_sort
                                            encoding:NSUTF8StringEncoding];
        start    = _start;
        count    = _count;
        context  = _context;
    }
    return self;
}

@end

/*----------------------------------------------------------------------
|   PLT_MediaServerSearchCapsule
+---------------------------------------------------------------------*/
@implementation PLT_MediaServerSearchCapsule

@synthesize search;

- (id)initWithAction:(PLT_Action*)action objectId:(const char*)_id search:(const char*)_search filter:(const char*)_filter start:(NPT_UInt32)_start count:(NPT_UInt32)_count sort:(const char*)_sort context:(PLT_HttpRequestContext*)_context
{
    if ((self = [super initWithAction:action
                             objectId:_id
                               filter:_filter
                                start:_start
                                count:_count
                                 sort:_sort
                              context:_context])) {
        search = [[NSString alloc] initWithCString:_search encoding:NSUTF8StringEncoding];
    }
    return self;
}

@end

/*----------------------------------------------------------------------
|   PLT_MediaServerFileRequestCapsule
+---------------------------------------------------------------------*/
@implementation PLT_MediaServerFileRequestCapsule

- (id)initWithResponse:(NPT_HttpResponse*)_response context:(PLT_HttpRequestContext*)_context
{
    if ((self = [super init])) {
        response = _response;
        context  = _context;
    }
    return self;
}

@end

/*----------------------------------------------------------------------
|   PLT_MediaServerObject
+---------------------------------------------------------------------*/
@interface PLT_MediaServerObject () {
	PLT_MediaServerDelegate_Wrapper *_wrapper;
}
@end

@implementation PLT_MediaServerObject

- (id)init 
{
	if ((self = [super init])) {
		PLT_MediaConnect* server = new PLT_MediaConnect("Test");
		PLT_DeviceHostReference _device(server);
		[self setDevice:&_device];

		_wrapper = new PLT_MediaServerDelegate_Wrapper(self);
        server->SetDelegate(_wrapper);
    }
    return self;
}

- (void)dealloc
{
    delete _wrapper;
}

@end


/*----------------------------------------------------------------------
 |   PLT_MediaRendererObject
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererObject () {
    PLT_MediaRendererDelegate_Wrapper *_wrapper;
}

@end

@implementation PLT_MediaRendererObject

- (id)init
{
    if ((self = [super init])) {
        PLT_MediaRenderer* server = new PLT_MediaRenderer("SofaPlay TV",false,"e6572b54-f3c7-2d91-2fb5-b757f2545e21");
        server->m_Manufacturer = "HighLVL";
        server->m_ModelDescription = "SofaPlay TV Renderer";
        server->m_ManufacturerURL = "http://highlvl.com/";
        server->m_ModelNumber = "1.0";
        server->m_ModelName = "SofaPlay TV Renderer";
        server->m_ModelURL = "http://highlvl.com/";

        PLT_DeviceHostReference _device(server);
        [self setDevice:&_device];
 
        _wrapper = new PLT_MediaRendererDelegate_Wrapper(self);
        server->SetDelegate(_wrapper);
        
        

    }
    return self;
}

- (PLT_Service*) findServiceByType: (NSString *)type
{
    PLT_Service* service;
    
    [self getDevice]->FindServiceByType([type cStringUsingEncoding:NSUTF8StringEncoding], service);
    
    return service;
    
}

- (void)dealloc
{
    
    delete _wrapper;
}

@end
