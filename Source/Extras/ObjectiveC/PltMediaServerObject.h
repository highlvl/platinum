//
//  PltMediaServerObject.h
//  Platinum
//
//  Created by Sylvain on 9/14/10.
//  Copyright 2010 Plutinosoft LLC. All rights reserved.
//

#import "Neptune.h"

#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
#include <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

#import "PltUPnPObject.h"
#import "PltService.h"

// define 
#if !defined(_PLATINUM_H_)
typedef class PLT_HttpRequestContext PLT_HttpRequestContext;
typedef class NPT_HttpResponse NPT_HttpResponse;
#endif

/*----------------------------------------------------------------------
|   PLT_MediaServerObject
+---------------------------------------------------------------------*/
@interface PLT_MediaServerObject : PLT_DeviceHostObject
@property (nonatomic, assign) id delegate;
@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererGeneralCapsule
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererGeneralCapsule : PLT_ActionObject {
}

- (id)initWithAction:(PLT_Action*)action;

@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererSetMuteCapsule
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererSetMuteCapsule : PLT_ActionObject {
}
@property (nonatomic, getter=isEnabled) BOOL desiredMute;

- (id)initWithAction:(PLT_Action*)action;

@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererSetAVTransportUriCapsule
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererSetAVTransportUriCapsule : PLT_ActionObject {
    NSString*   uri;
}

- (id)initWithAction:(PLT_Action*)action;

@property (readonly, copy) NSString* uri;

@end


/*----------------------------------------------------------------------
 |   PLT_MediaRendererSeekCapsule
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererSeekCapsule : PLT_ActionObject {
    NSString*   target;
}

- (id)initWithAction:(PLT_Action*)action;

@property (readonly, copy) NSString* target;

@end




/*----------------------------------------------------------------------
|   PLT_MediaServerBrowseCapsule
+---------------------------------------------------------------------*/
@interface PLT_MediaServerBrowseCapsule : PLT_ActionObject {
    NSString*               objectId;
    NPT_UInt32              start;
    NPT_UInt32              count;
    NSString*               filter;
    NSString*               sort;
    PLT_HttpRequestContext* context;
}

- (id)initWithAction:(PLT_Action*)action objectId:(const char*)objectId filter:(const char*)filter start:(NPT_UInt32)start count:(NPT_UInt32)count sort:(const char*)sort context:(PLT_HttpRequestContext*)context;

@property (readonly, copy) NSString* objectId;
@property (readonly) NPT_UInt32 start;
@property (readonly) NPT_UInt32 count;
@property (readonly, copy) NSString* filter;
@property (readonly, copy) NSString* sort;
@end

/*----------------------------------------------------------------------
|   PLT_MediaServerSearchCapsule
+---------------------------------------------------------------------*/
@interface PLT_MediaServerSearchCapsule : PLT_MediaServerBrowseCapsule {
    NSString* search;
}

- (id)initWithAction:(PLT_Action*)action objectId:(const char*)objectId search:(const char*)search filter:(const char*)filter start:(NPT_UInt32)start count:(NPT_UInt32)count sort:(const char*)sort context:(PLT_HttpRequestContext*)context;

@property (readonly, copy) NSString* search;
@end

/*----------------------------------------------------------------------
|   PLT_MediaServerFileRequestCapsule
+---------------------------------------------------------------------*/
@interface PLT_MediaServerFileRequestCapsule : NSObject {
    NPT_HttpResponse*       response;
    PLT_HttpRequestContext* context;
}

- (id)initWithResponse:(NPT_HttpResponse*)response context:(PLT_HttpRequestContext*)context;
@end

/*----------------------------------------------------------------------
|   PLT_MediaServerDelegateObject
+---------------------------------------------------------------------*/
@protocol PLT_MediaServerDelegateObject
- (NPT_Result)onBrowseMetadata:(PLT_MediaServerBrowseCapsule*)info;
- (NPT_Result)onBrowseDirectChildren:(PLT_MediaServerBrowseCapsule*)info;
- (NPT_Result)onSearchContainer:(PLT_MediaServerSearchCapsule*)info;
- (NPT_Result)onFileRequest:(PLT_MediaServerFileRequestCapsule*)info;
@end

/*----------------------------------------------------------------------
 |   PLT_MediaServerObject
 +---------------------------------------------------------------------*/
@interface PLT_MediaRendererObject : PLT_DeviceHostObject
@property (nonatomic, assign) id delegate;

- (PLT_Service*) findServiceByType: (NSString *)type;

@end

/*----------------------------------------------------------------------
 |   PLT_MediaRendererDelegateObject
 +---------------------------------------------------------------------*/
@protocol PLT_MediaRendererDelegateObject
- (void)onConnectionEstablished:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onGetCurrentConnectionInfo:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onPlay:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onStop:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onPause:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onSeek:(PLT_MediaRendererSeekCapsule*)info;
- (NPT_Result)onSetMute:(PLT_MediaRendererSetMuteCapsule*)info;
- (NPT_Result)onSetPlayMode:(PLT_MediaRendererGeneralCapsule*)info;
- (NPT_Result)onSetAVTransportUri:(PLT_MediaRendererSetAVTransportUriCapsule*)info;
@end

